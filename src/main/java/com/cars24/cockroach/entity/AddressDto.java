package com.cars24.cockroach.entity;

import lombok.Data;

import javax.persistence.Column;

@Data
public class AddressDto {
    private Long addressId;
    private String addressLine;
    private String city;
    private String state;
    private Long pinCode;
}
