package com.cars24.cockroach.entity;

import lombok.Data;

import java.util.List;

@Data
public class UserDto {

    private String firstName;
    private String lastName;
    private AddressDto address;
}


