package com.cars24.cockroach.entity;

import com.cars24.cockroach.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public void deductMoney(int money, long userId){

       User user = userRepository.findById(userId).get();

       if (user != null){
           if (user.getAmount() > money){
               user.setAmount(user.getAmount() - money);
           }
       }
    }

    @Transactional
    public Long getMoney(long userId){

        User user = userRepository.findById(userId).get();

        if (user != null){
            return user.getAmount();
        }

        return -1L;
    }
}
