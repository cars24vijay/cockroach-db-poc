package com.cars24.cockroach.controller;


import com.cars24.cockroach.entity.Address;
import com.cars24.cockroach.entity.UserDto;
import com.cars24.cockroach.entity.User;
import com.cars24.cockroach.entity.UserService;
import com.cars24.cockroach.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    UserService userService;

    @PostMapping("/user/add")
    public ResponseEntity addUser(@RequestBody UserDto userRequest) {
        log.info("user:"+userRequest);

        Address address = Address.builder().adressLine(userRequest.getAddress().getAddressLine()).city(userRequest.getAddress().getCity()).state(userRequest.getAddress().getState()).build();
        List<Address> addressList = new ArrayList();
        addressList.add(address);
        //User user = User.builder().firstName(userRequest.getFirstName()).lastName(userRequest.getLastName()).address(addressList).build();

        userRepository.save(new User());
        return new ResponseEntity<>("Successfully added", HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<Long> getUser(@PathVariable Long userId) {
        userService.deductMoney(1,userId);
        return new ResponseEntity<>(userService.getMoney(userId), HttpStatus.OK);
    }
}
